## Description

In this project a web application based on microservices is auto deployed into k8s on every commit to this reposotory, Deployed applcation microservices fetches creadentials from the vault cluster running inside K8s cluster to access mongoDB database and fetches data from the mongoDB Database runnning inside K8s cluster.

![image](AutoDeploymentUsingGitlab&traceMonitoringUsingJaeger.png)
<br/>

The main pupose of this project is to learn Otel instrumentation in an application and sending tracing data to a tracing backend (In this application e.g Jaeger) to application visualization.

### Technologies (Used in this project)

- Aws EKS (Elastic Kubernetes Service).
- Istio (Service Mesh)
- Docker (Container Tech)
- Golang (All micro-services)
- Gitlab CI/CD
- Hashicorp Vault (Secret Manager)
- Terraform (Infrastructure As Code)
- Open Telemetry (Used to instrument micro-services to emit traces)
- Jaeger (Traces Backend)

### Overview

In this project a web application based on microservices is auto deployed into k8s with the help of Gitlab CI/CD pipeline on every commit to this reposotory, Deployed applcation microservices fetches creadentials from the Hashicorp vault cluster running inside K8s cluster to access mongoDB database and fetches data from the mongoDB Database runnning inside K8s cluster.

And Istio service mesh manages the traffic that comes for the application based on path like giving access to the traffic with relevant path and blocking access for irrelevant path. And also monitors traffic among microservices.

And Running application send traces to the Jaeger agent in the same namespace backend.

### In-depth Explaination:

First I’ll start with explaining the setting up infra for this Poject, And for this I used terraform to set up EKS in an AWS VPC.

When the Cluster was up & running then I connected Gitlab with my K8s cluster by deploying a Gitlab agent in one of the cluster’s namespace to give Gitlab required permissions to operate my cluster automatically.

Then I installed Istio service mesh, Jaeger operator, Hashicorp Vault, MongoDB Operator in K8s cluster.

Then I started setting up Istio service mesh by applying Gateway, and VirtualService components in cluster that manage the incoming traffic into Cluster also labeled application namespace so that Istio control-plane can start Envoy-proxy as sidecar within microservice pods.

Then I set up MongoDB in cluster by creating MongoDBCommunity resource attached with PVC in cluster.

Then I set up Jaeger operator by creating a Jaeger instance in the application namespace which recieves tracing data from the application microservices.

Then the last tool that I set up in cluster was Hashicorp vault to hold my MongoDB password and to allow a serviceAccount to fetch those credentials so that using that serviceAccount running application in cluster can fetch credentials from Vault.

After configuring all the useful tools & Databse in Cluster I created CI/CD pipeline to perform some minimal Tests, Build image & push to registry and deploy to K8s cluster.

So On every commit to master branch pipeline runs and deploy micro-services to K8s cluster, And In cluster running microservices fetches credentials from Vault and on every incoming request application generates traces that it sends to Jaeger backend.



### Pipeline Flow

On Every commit to this repo followings happen sequencely:

- microservices container images are built and pushed to [my dockerhub repository](https://hub.docker.com/repository/docker/mdsahiloss/jaeger-tracing/general)
- Then container images of deployments are updated in rolling-update mode to achieve **zero down time** inside k8s cluster running as AWS EKS.

### Application Workflow inside K8s

Once application gets deployed inside K8s cluster then followings happen on access request to the application using http protocol:

- Appication microservices fetches credentials from Vault cluster running inside K8s to access MongoDB Database.
- And fetches data from the MongoDB.
- And send tracing data to Jaeger agent running in the same namespace of K8s cluster

### Proof of project 

![working-video](tracing-working-video.mp4)

## Microservices

Following explaining about each microservice which is being used in this application:

### A simple Web UI: videos-web

<hr/>
<br/>

Consider `videos-web` <br/>
It's an HTML application that lists a bunch of playlists with videos in them.

```
+------------+
| videos-web |
|            |
+------------+
```

<br/>

### A simple API: playlists-api

<hr/>
<br/>

For `videos-web` to get any content, it needs to make a call to `playlists-api`

```
+------------+     +---------------+
| videos-web +---->+ playlists-api |
|            |     |               |
+------------+     +---------------+

```

Playlists consist of data like `title`, `description` etc, and a list of `videos`. <br/>
Playlists are stored in a database. <br/>
`playlists-api` stores its data in a database

```
+------------+     +---------------+    +--------------+
| videos-web +---->+ playlists-api +--->+ playlists-db |
|            |     |               |    |              |
+------------+     +---------------+    +--------------+

```

<br/>

### A little complexity

<hr/>
<br/>

Each playlist item contains only a list of video id's. <br/>
A playlist does not have the full metadata of each video. <br/>

Example `playlist`:

```
{
  "id" : "playlist-01",
  "title": "Cool playlist",
  "videos" : [ "video-1", "video-x" , "video-b"]
}
```

Take not above `videos: []` is a list of video id's <br/>

Videos have their own `title` and `description` and other metadata. <br/>

To get this data, we need a `videos-api` <br/>
This `videos-api` has its own database too <br/>

```
+------------+       +-----------+
| videos-api +------>+ videos-db |
|            |       |           |
+------------+       +-----------+
```

For the `playlists-api` to load all the video data, it needs to call `videos-api` for each video ID it has.<br/>
<br/>

## Traffic flow

<hr/>
<br/>
A single `GET` request to the `playlists-api` will get all the playlists 
from its database with a single DB call <br/>

For every playlist and every video in each list, a separate `GET` call will be made to the `videos-api` which will
retrieve the video metadata from its database. <br/>

This will result in many network fanouts between `playlists-api` and `videos-api` and many call to its database. <br/>
This is intentional to demonstrate a busy network.

<br/>

## Full application architecture

<hr/>
<br/>

```

+------------+     +---------------+    +--------------+
| videos-web +---->+ playlists-api +--->+ playlists-db |
|            |     |               |    |   [mongoDB]  |
+------------+     +-----+---------+    +--------------+
                         |
                         v
                   +-----+------+       +-----------+
                   | videos-api +------>+ videos-db |
                   |            |       | [mongoDB] |
                   +------------+       +-----------+

```

<br/>

## Run the apps: Docker

<hr/>
<br/>
There is a `docker-compose.yaml`  in this directory. <br/>
Change your terminal to this folder and run:

```
cd tracing

docker-compose build

docker-compose up

```

You can access the app on `http://localhost`. <br/>
You should now see the complete architecture in the browser
<br/>

## Traces

<hr/>
<br/>

To see our traces, we can access the Jaeger UI on `http://localhost:16686`
